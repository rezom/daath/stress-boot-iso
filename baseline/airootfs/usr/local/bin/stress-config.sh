#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

my_device="/dev/${1##*/}"
my_config_mount_dir='/tmp/stress-config-mount'
my_config_lockfile='/tmp/stress-config.lock'
my_run_lockfile='/tmp/stress-run.lock'
my_run_workdir='/tmp/stress-workdir'
my_run_logfile='output.log'
my_config_filenames=('mprime.config' 'stress-ng.config' 'stress.config' 'systester-cli.config')

my_mount_config() {
	umount "${my_config_mount_dir}" 2>/dev/null || true
	mkdir -p "${my_config_mount_dir}"
	mount -o ro "${my_device}" "${my_config_mount_dir}"
}

my_umount_config() {
	umount -l "${my_config_mount_dir}" || true
	rmdir "${my_config_mount_dir}" || true
}

my_load_config() {
	(
		flock --timeout 20 4
		my_mount_config
		for f in "${my_config_filenames[@]}"
		do
			if [[ -f "${my_config_mount_dir}/${f}" ]]
			then
				echo "${f}"
				cat "${my_config_mount_dir}/${f}" 2>/dev/null || true
				# we append a random character since trailing newlines get trimmed
				echo -n x
				break
			fi
		done
		my_umount_config
	) 4>"${my_config_lockfile}"
}

my_main() {
	local config
	local configname
	config="$(my_load_config)"
	[[ -z "${config}" ]] && return 0
	configname="$(echo -n "${config}" | head -n 1)"
	config="$(echo -n "${config}" | tail -n +2)"
	[[ -z "${config%?}" ]] && return 0

	exec 4<>"${my_run_lockfile}"
	flock --nonblock 4
	rm -rf "${my_run_workdir}"
	mkdir -p "${my_run_workdir}"
	cd "${my_run_workdir}"
	case "${configname}" in
		'mprime.config')
			echo -n "${config%?}" | mprime >> "${my_run_logfile}" 2>&1
			;;
		'stress-ng.config')
			echo "${config%?}" | xargs stress-ng >> "${my_run_logfile}" 2>&1
			;;
		'stress.config')
			echo "${config%?}" | xargs stress >> "${my_run_logfile}" 2>&1
			;;
		'systester-cli.config')
			echo "${config%?}" | xargs systester-cli >> "${my_run_logfile}" 2>&1
			;;
	esac
}

flock --nonblock "${my_run_lockfile}" true || exit 0
my_main
