# Using Archiso

https://wiki.archlinux.org/index.php/Archiso

## Generate `.iso`

Adjust `baseline/pacman.conf` :
```
...
[customrepo]
SigLevel = Optional TrustAll
Server = file:///customrepo
```
Make `Server` point to the absolute `customrepo` folder path.

Generate image as root :
```
# mkarchiso -v baseline
```

The resulting iso will be placed into `out/`

## SSH

Connect to root using `root` password.

## Initial setup

CPU is set to maximum performance using the performance governor.
Also performance bias is set to 0 for maximum power use if available.

cpupower config location : `baseline/airootfs/etc/default/cpupower`

A udev rule is used to invoke a script when a device with filesystem is attached.

It will look for a config file in this order :
  - `mprime.config` : `mprime` (https://www.mersenne.org/)
  - `stress-ng.config` : `stress-ng` (https://manpages.ubuntu.com/manpages/artful/man1/stress-ng.1.html)
  - `stress.config` : `stress` (https://linux.die.net/man/1/stress)
  - `systester-cli.config` : `systester-cli` (http://systester.sourceforge.net/)

Udev rule is : `baseline/airootfs/etc/udev/rules.d/99-stress-config.rules`

It invokes : `baseline/airootfs/usr/local/bin/stress-config.sh`

## `mprime.config`

See : https://www.mersenne.org/

When invoking `mprime` it will query some inputs :

```
Join Gimps? (Y=Yes, N=Just stress testing) (Y): N
Number of torture test threads to run (8): 
Choose a type of torture test to run.
  1 = Smallest FFTs (tests L1/L2 caches, high power/heat/CPU stress).
  2 = Small FFTs (tests L1/L2/L3 caches, maximum power/heat/CPU stress).
  3 = Large FFTs (stresses memory controller and RAM).
  4 = Blend (tests all of the above).
Blend is the default.  NOTE: if you fail the blend test but pass the
smaller FFT tests then your problem is likely bad memory or bad memory
controller.
Type of torture test to run (4): 2
Customize settings (N): 
Run a weaker torture test (not recommended) (N): 
Accept the answers above? (Y): 
```

Create a `mprime.config` in the root of a partition you are going to plug into the system.
The config should mirror the `mprime` user input like this (correctly specify the newlines) : 

```
N

2




```

Empty lines mean the default value will be used.
This config will use the default number of threads and run torture test 2.

```
N
4
1




```
This one will use 4 threads and run torture test 1.

## `stress-ng.config`

`stress-ng` has a lot of options. See : https://manpages.ubuntu.com/manpages/artful/man1/stress-ng.1.html

The config file should contain the command line options to pass. They can be on multiple lines for better readability.

Create a `stress-ng.config` file in the root of a partition you are going to plug into the system.

```
-c 0 -l 40
```
This example will impose a 40% load on all cpus.

## `stress.config`

`stress` is a standard stress tool. See : https://linux.die.net/man/1/stress

The config file should contain the command line options to pass. They can be on multiple lines for better readability.

Create a `stress.config` file in the root of a partition you are going to plug into the system.

```
-c 4 -t 60
```
This example will create 4 threads and timeout after 60 seconds.

## `systester-cli`

Pi calculations. See : http://systester.sourceforge.net/

Command line switches :
```
 -help                  Prints this help screen
 -qcborwein <digits>    Use Borwein's Quadratic Convergence algorithm
 -gausslg  <digits>     Use Gauss-Legendre algorithm
  Valid <digits> values: 128K, 256K, 512K, 1M, 2M, 4M, 8M, 16M, 32M, 64M, 128M

 -threads <nthreads>    The number of threads. Default 1, Maximum 64
 -turns <nturns>        The number of turns. Default 1
 -test                  Set operating mode to Test. This is the default
 -bench                 Set operating mode to Bench
 -log                   Create the systester.log file
 -pifile                Create the CPUPI.DAT file
```

The config file should contain the command line options to pass. They can be on multiple lines for better readability.

Create a `systester-cli.config` file in the root of a partition you are going to plug into the system.

```
-gausslg 1M -threads 2 -bench
```

